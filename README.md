# Sistema de Comunicados - Prova Before

Olá, tudo bem? Esse projeto foi desenvolvido em angular 8 e o mesmo está disponível no link: [prova-before](https://prova-before.omateus.me)

## Executando o projeto

Após fazer o fork do projeto na sua máquina, você deve executar os seguintes comandos.

###### Comando 1: npm run server no servidor
###### Comando 2: npm install
###### Comando 3: ng serve

## Caso queira verificar o meu lint

Muito simples, vamos começar com o passo de cima, execute o npm install e após isso dê o seguinte comando.

###### Comando: ng lint

## Para filtrar por categorias

Tentei facilitar o máximo possível, para filtrar por categorias você pode simplesmente clicar em uma categoria ali já listada, porém, pode executar o segundo passo também, que é simplesmente clicando no ícone do menu e escolhendo a categoria que mais lhe agrada.

## Busca

O sistema de busca é muito simples, e você pode testa-lo clicando no ícone de lupa que se encontra no topo direito da sua tela.

## Setando como lido

Ao clicar em qualquer comunicado, o mesmo fica salvo no cache do seu navegador e mesmo reiniciando ou recarregando a página continua sendo marcado o que você já leu.

## Adicionais

Caso precise de mais alguma informação estou a disposição para sanar quaisquer que sejam.

###### Informações de Contato

**Blog:** [omateus.me](https://omateus.me)

**E-mail:** [nesantana15@gmail.com](mailto:nesantana15@gmail.com)

**Celular:** [(67) 9 8151-3750](tel:67981513750)
