import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class ApiComunicadosService {

    url = 'http://localhost:84/';

    constructor(private http: HttpClient) {
    }

    allComunicados() {
        return this
            .http
            .get(this.url + 'comunicados', {});
    }

    tiposComunicados() {
        return this
            .http
            .get(this.url + 'tiposComunicados', {});
    }
}
