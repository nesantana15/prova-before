import { TestBed } from '@angular/core/testing';

import { ApiComunicadosService } from './api-comunicados.service';

describe('ApiComunicadosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApiComunicadosService = TestBed.get(ApiComunicadosService);
    expect(service).toBeTruthy();
  });
});
