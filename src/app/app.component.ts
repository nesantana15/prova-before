import {Component, OnInit} from '@angular/core';
import {ApiComunicadosService} from './api/api-comunicados.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

    title = 'comunicados';
    allComunicados: any = [];
    tiposComunicados: any = [];
    term: any = {};
    buscaAberta = false;
    urlApi: string;
    navAberto = false;
    tipoAtivo = null;
    comunicadosLidos: any = [];
    selecioneiComunicado = false;
    comunicadoSelecionado: any = {};

    ngOnInit() {
        this.pegaComunicados(() => {});
        this.pegaTipos();
        this.urlApi = this.api.url;
        if (localStorage.getItem('comunicados_lidos') === null) {
            localStorage.setItem('comunicados_lidos', null);
        } else {
            this.comunicadosLidos = localStorage.getItem('comunicados_lidos').split(',');
        }
    }

    constructor(private api: ApiComunicadosService) {
    }

    pegaComunicados(callback) {
        this.api.allComunicados().subscribe((res) => {
            this.allComunicados = res;
            callback();
        });
    }

    pegaTipos() {
        this.api.tiposComunicados().subscribe((res) => {
            this.tiposComunicados = res;
        });
    }

    abreBusca() {
        this.buscaAberta = true;
    }
    fechaBusca() {
        this.buscaAberta = false;
    }

    abreNavStyle() {
        this.navAberto = true;
    }

    fechaNavStyle() {
        this.navAberto = false;
    }

    achaTipo(idTipo) {
        let name;
        this.tiposComunicados.filter((tipos) => {
            if (tipos.type === idTipo) {
                name = tipos.name;
            }
        });
        return name;
    }

    trocaTipo(idTipo) {
        this.pegaComunicados(() => {
            this.tipoAtivo = idTipo;
            const comunicadosDaqueleTipo: any = [];
            this.allComunicados.filter((comunicado) => {
                if (comunicado.type === idTipo) {
                    comunicadosDaqueleTipo.push(comunicado);
                }
            });
            this.allComunicados = comunicadosDaqueleTipo;
            this.navAberto = false;
        });
    }

    limpaTipos() {
        this.tipoAtivo = null;
        this.pegaComunicados(() => {
        });
    }

    selecionaComunicado(comunicado) {
        this.comunicadoSelecionado = comunicado;
        this.selecioneiComunicado = true;

        if (localStorage.getItem('comunicados_lidos') === 'null') {
            this.comunicadosLidos.push(comunicado.id);
            localStorage.setItem('comunicados_lidos', this.comunicadosLidos.toString());
        } else {
            let jaLeu = false;
            this.comunicadosLidos = localStorage.getItem('comunicados_lidos').split(',');
            this.comunicadosLidos.filter((cadaId) => {
                if (comunicado.id === cadaId) {
                    jaLeu = true;
                }
            });
            if (jaLeu === false) {
                this.comunicadosLidos.push(comunicado.id.toString());
                localStorage.setItem('comunicados_lidos', this.comunicadosLidos.toString());
            }
        }
    }

    comunicado_lido(id) {
        let foiLido = false;
        this.comunicadosLidos.filter((cadaComunicado) => {
            if (Number(cadaComunicado) === id) {
                foiLido = true;
            }
        });
        if (foiLido) {
            return true;
        } else {
            return false;
        }
    }

    limpa_comunicado() {
        this.comunicadoSelecionado = {title: ''};
        this.selecioneiComunicado = false;
    }

}
